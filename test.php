
<html>
<body>
<h4>1.Boolval:</h4>
<?php
echo '0: '.(boolval(0) ? 'true' : 'false');
?>
<br>
<h4>Empty:</h4>
<?php
$a=null;

if (empty($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=3;

if (empty($b)){
	echo "true";
}
else{
	echo "false";
}
?>
<h4>2.Floatval:</h4>
<?php
$a="12.34gf";
echo floatval($a);
echo "<br>";
$b="gf12.34";
echo floatval($b);
?>
<br>
<h4>3.Is_array:</h4>
<?php
$a=array("this","is","an array");
if (is_array($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b="this is not an array";
if (is_array($b)){
	echo "true";
}
else{
	echo "false";
}

?>
<br>
<h4>4.Is_bool:</h4>
<?php
$a=true;
if (is_bool($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=22;
if (is_bool($b)){
	echo "true";
}
else{
	echo "false";
}

?>

<h4>5.Is_int:</h4>
<?php
$a=2345;
if (is_int($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=array("pavel",23.45,45);
if (is_int($b)){
	echo "true";
}
else{
	echo "false";
}

?>
<h4>6.Is_null:</h4>
<?php
$a="pavel";
if (is_null($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=null;
if (is_null($b)){
	echo "true";
}
else{
	echo "false";
}

?>
<h4>7.Is_null:</h4>
<?php
$a="pavel";
if (is_null($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=null;
if (is_null($b)){
	echo "true";
}
else{
	echo "false";
}

?>
<h4>8.Is_string:</h4>
<?php
$a="pavel";
if (is_string($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=null;
if (is_string($b)){
	echo "true";
}
else{
	echo "false";
}
?>
<h4>9.Is_object:</h4>
<?php
function get_students($obj)
{
    if (!is_object($obj)) {
        return false;
    }

    return $obj->students;
}

$obj = new stdClass();
$obj->students = array('Kalle', 'Ross', 'Felipe');

print_r(get_students(null));
print_r(get_students($obj));
?> 
<h4>10.Isset:</h4>
<?php
$a="pavel";
if (isset($a)){
	echo "true";
}
else{
	echo "false";
}
echo "<br>";
$b=null;
if (isset($b)){
	echo "true";
}
else{
	echo "false";
}
?>
<h4>11.Unset:</h4>
<?php
$a="pavel";
unset($a);
if (empty($a)){
	
	echo "unset success";
}
?>
<h4>12.Print_r:</h4>
<pre>
<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r ($a);
?>
</pre> 
<h4>14.Var_dump:</h4>
<?php
$a = array(1, 2, array("a", "b", "c"));
var_dump($a);
?> 
</body>
</html>